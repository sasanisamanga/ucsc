#include<stdio.h>
int main()
{
    int x,y,a;
    printf("Input two integers : \n");
    scanf("%d%d",&x,&y);

    printf("Before swapping:\n First Integer Number = %d\n Second Integer Number = %d\n",x,y);

    a=x;
    x=y;
    y=a;

    printf("After swapping:\n First Integer Number = %d\n Second Integer Number = %d\n",x,y);
  return 0;
}

